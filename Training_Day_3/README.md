---

## 1. Write a program to reverse a number.

### Using loop

```python
num = int(input("Enter number to be reversed "))
rev_num = 0
while num:
    rev_num *= 10
    rev_num += num%10
    num //= 10
print("Reversed number is", rev_num)
```

### Using recursion

```python
def reverseNum(num, rev = 0):
    if num == 0:
        return rev
    return reverseNum(num//10, rev*10 + num%10)

num = int(input("Enter number to be reversed "))
rev_num = reverseNum(num)
print("Reversed number is", rev_num)
```

### Using string

```python
num = int(input("Enter number to be reversed "))
num_str = str(num)
rev_num = int(num_str[::-1])
print("Reversed number is", rev_num)
```

### Best Solution(s):

- **Using loop:** This solution is easy to understand and doesn’t need conversion from int to string and vice-versa.

### Other Solutions’ explanation:

- **Using recursion:** This solution is reduces code readability and also requires to maintain an unnecessary recursion stack.
- **Using string:** This solution is the most readable in terms of code but requires conversion from int to string and then again from string to int (string to int conversion needed if the reverse number is further required to be used as an int).

---

## 2. Write a program to reverse a string.

### Using loop

```python
s = input("Enter string to reverse ")
rev_str = ""
for i in s:
    rev_str = i + rev_str
print("Reversed string is", rev_str)
```

### Using recursion

```python
def reverse(s):
    if len(s) == 1:
        return s
    return reverse(s[1:]) + s[0]

s = input("Enter string to reverse ")
rev_str = reverse(s)
print(rev_str)
```

### Using stack

```python
s = input("Enter string to reverse ")
rev_str = ""
L = []
for i in s:
    L.append(i)
while L:
    rev_str += L.pop()
print("Reversed string is",rev_str)
```

### Using slice

```python
s = input("Enter string to reverse ")
rev_str = s[::-1]
print("Reversed string is", rev_str)
```

### Using built-in (reversed()) function

```python
s = input("Enter string to reverse ")
rev_str = "".join(reversed(s))
print("Reversed string is", rev_str)
```

### Best Solution(s):

- **Using built-in reversed() function:** The reversed() function receives a sequence and return an iterator pointing to the last position of the sequence. It does not create a copy of the original string/sequence and simply returns an iterator which can be used to iterate over the passed sequence. For more clarification, [visit this link](https://www.geeksforgeeks.org/python-reversed-vs-1-which-one-is-faster/) which clarifies why reversed() is better than slice for larger string.

### Other Solutions’ explanation:

- **Using loop:** This solution iterates over the entire string character by character and then adds the new character to the previously stored reversed string. The code readability for this solution is lesser than the other solutions mentioned above, like slicing and reversed().
- **Using recursion:** This solution uses a recursive technique to reverse the string. The code readability and simplicity is the worst among all the solutions. Also, an extra recursion stack is maintained in the background.
- **Using list as stack:** This solution iterates over the entire string and puts every character into a stack and then pops the stack to create a new string.
- **Using slice:** This solution uses string slicing to create a new reversed string. Slicing solution increases the code readability and is very easy to understand but still not better than the reversed() solution, since it creates a new copy of the string whereas reversed() just returns an iterator.

---

## 3. Write a program to reverse a list.

### Using builtin reversed() function

```python
def reverseList(L):
    return [item for item in reversed(L)]
L = input("Enter list elements to reverse ").split()
print(reverseList(L))
```

### Using builtin reverse() function

```python
L = input("Enter list elements to reverse ").split()
L.reverse()
print(L)
```

### Using slice

```python
L = input("Enter list elements to reverse ").split()
rev_list = L[::-1]
print(rev_list)
```

### Best Solution(s):

**Depends on requirements**

- **Using reverse() function:** The reverse() function does the reversal in-place but **modifies the original list.** Therefore, in conditions where we need to modify the original list, this is the best solution for reversal.
- **Using reversed() function:** The reversed() function returns an iterator for the reversed iteration of the list. This doesn’t do an in-place reversal rather we can reversely iterate over the list and generate a new one. Therefore, it is best suited in situations where modification of the original list is not required.

### Other Solutions’ explanation:

- **Using slice:** The slice method returns a new list. Therefore it create a copy of the original list but the reversed() function still performs better than slice.

---

## 4. Write a Program to count the number of vowels in a String.

### Using if-else checks

```python
s = input("Enter string to count vowels ")
count = 0
for i in s.lower():
    if i == 'a' or i == 'e' or i == 'i' or i == 'o' or i == 'u':
        count += 1
print(count)
```

### Using set

```python
def countVowels(s):
    count = 0
    for i in s:
        if i in vowels:
            count += 1
    return count
vowels = set("aeiouAEIOU")
s = input("Enter string to count vowels ")
print(countVowels(s))
```

### Best Solution(s):

- **Using set:** This solution increases code readability and is easy to understand, since it creates a vowel set and checks for every character presence in vowel set.

### Other Solutions’ explanation:

- **Using if-else checks:** This solution makes the code a little less readable.

---

## 5. Write a program to remove a given character from a string.

### Using replace

```python
s = input("Enter string ")
ch = input("Enter character to remove ")
print(s.replace(ch,''))
```

### Using translate

```python
s= input("Enter string ")
ch = input("Enter character to remove ")
print(s.translate({ord(ch) : None}))
```

### Best Solution(s):

**Depends on situation**

- **Using replace:** This solution can accept a sub-string to be replaced in the string and also provides a third parameter of number of times to remove the sub-string.
- **Using translate:** This solution replaces the occurrence of every character by replacing its Unicode to None.

---

## 6. Write a Program to count occurrences of a given character in a string.

### Using loop

```python
s = input("Enter string ")
ch = input("Enter char to count ")
count = 0
for i in s:
     if i == ch:
        count += 1
print(count)
```

### Using count()

```python
s = input("Enter string ")
ch= input("Enter char to count ")
count = s.count(ch)
print(count)
```

### Using Counter()

```python
from collections import Counter
s = input("Enter string ")
ch = input("Enter char to count ")
count = Counter(s)
print(count[ch])
```

### Using lambda with sum and map

```python
s = input("Enter string ")
ch = input("Enter char to count ")
count = sum(map(lambda x : 1 if ch in x else 0, s))
print(count)
```

### Using re.findall()

```python
import re
s = input("Enter string ")
ch = input("Enter char to count ")
count = len(re.findall(ch, s))
print(count)
```

### Best Solution(s):

- **Using count():** This solution returns the count of the passed parameter in the provided sequence(list, string etc). It increases the code readability and is very easy to understand.

### Other Solutions’ explanation:

- **Using loop:** This solution iterates over the entire string character by character and then counts the occurrence of the char in the string. Other builtin functions also use the same mechanism under the hood but are more readable and this solution increases line of code when a similar alternative is already present.
- **Using Counter():** This solution return a dictionary with all the characters and their count in the string. Since, in this particular case we are required to count only one character, a simple str.count() is a better alternative.
- **Using lambda with sum and map:** This solution also checks for every character of the string s. This reduces lines of code but still not a better alternative against count().
- **Using re.findall():** This solution uses regular expressions to count the char occurrence in a string.

---

## 7. Write a Program to remove duplicates from the list.

### Using loop and creating a new list

```python
L = [1,2,3,1,1,1,1,2,3,4]
ans = []
for i in L:
    if i not in ans:
        ans.append(i)
print(ans)
```

### Using list comprehension

```python
def removeDuplicates(L):
    ans = []
    [ans.append(x) for x in L if x not in ans]
    return ans

L = [1,2,3,1,2,3,1,23,23,1,100]
print(removeDuplicates(L))
```

### Using set

```python
L = [1,2,3,1,2,3,1,23,23,1,100]
print(list(set(L)))
```

### Using collections.OrderedDict.fromkeys()

```python
from collections import OrderedDict
L = [1,2,3,1,2,3,1,23,23,1,100]
ans = list(OrderedDict.fromkeys(L))
print(ans)
```

### Best Solution(s):

- **Using collections.OrderedDict.fromkeys():** This solution is the fastest among all the solutions and it works by creating a dictionary of unique elements in the list. We can proceed by creating a list out of the received dictionary.

### Other Solutions’ Explanation:

- **Using loop and creating a new list:** This solution iterates over the whole list and creates a new list with only unique elements.
- **Using list comprehension:** This is exactly similar to the above solution but is written in a one-liner shorthand technique to improve code readability.
- **Using set:** This solution creates a set of all the elements in the list and therefore removes duplicacy. The problem with this solution is that the original ordering of the elements in the list is lost.

---

## 8. Write a Program to find which number is not present in the second list.

### Using loops

```python
A = [1,2,3,4,5,6,7,8,9,10]
B = [2,5,7,8]
for i in A:
    if i not in B:
        print(i,end=" ")
```

### Using hashing

```python
A = [1,2,3,4,5,6,7,8,9,10]
B = [2,5,7,8]
m = len(B)
n = len(A)
s = dict()
for i in B:
    s[i] = 1
for i in A:
    if i not in s.keys():
        print(i, end=" ")
```

### Best Solution(s):

- **Using hashing:** This solution has a TC of O(N+m) and a SC of O(m), where n is the size of list one and m is the size of list two. Since this is the fastest solution, it is considered as the best.

### **Other Solutions’ Explanation:**

- **Using loops:** This solution has a TC of O(n * log(m)) and a SC of O(1). Since this solution is not faster than hashing, it is not considered as the best solution. But the SC of this solution in O(1).

---

## 9. Write a Program to swap the first and last item of a list.

### Using size of list

```python
a = [1,2,3,4,5,6,7]
size = len(a)
temp = a[0]
a[0] = a[size-1]
a[size-1] = temp
print(a)
```

### Using indexing

```python
a = [1,2,3,4,5,6,7]
a[0], a[-1] = a[-1], a[0]
print(a)
```

### Using tuple unpacking

```python

a = [1,2,3,4,5,6,7]
ele = a[-1], a[0]
a[0], a[-1] = ele
print(a)
```

### Best Solution(s):

- **Using indexing:** This solution provides a one-liner shorthand for swapping two elements and makes the code readable and easier to understand.

### Other Solutions’ Explanation:

- **Using size of list:** This solution swaps the list elements using a third variable and the size of the list.
- **Using tuple unpacking:** This solution packs the first and -1 index of list into and tuple and then unpacks them into the opposite indexes, thus swapping the values.

---

## 10. Write a Program to check common characters in two given strings.

### Using loops

```python
a = "thisisstringone"
b = "stringtwo"
s = set()
for i in a:
    if i in b:
        s.add(i)
print(s)
```

### Using dictionary

```python
a = "thisisstringone"
b = "stringtwo"
map = dict()
for i in a:
    map[i] = 1
for i in b:
    if i in map.keys():
        map[i] = 2
for key, val in map.items():
    if val == 2:
        print(key, end=" ")
```

### Best Solution(s):

- **Using dictionary:** This solution uses a dictionary and hence is faster than the solution using loops.

### Other Solutions’ Explanation:

- **Using loops:** This approach iterates over the entire string and then searches for every element in the next string. Therefore, it is comparatively slower than alternative solution.

---