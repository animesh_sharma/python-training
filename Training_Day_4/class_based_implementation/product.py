class Product:
    '''This is the Product class
    
    It contains details for individual product
    '''

    def __init__(self, name, cost_price, country):
        self.sales_tax = 0.18
        self.name = name
        self.cost_price = cost_price
        self.country = country
        self.final_price = self.sales_tax * int(cost_price) + int(cost_price)
