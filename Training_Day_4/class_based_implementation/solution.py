from logistics import Logistics


def main():
    logi = Logistics()
    logi.generate_logistics()

if __name__ == "__main__":
    main()