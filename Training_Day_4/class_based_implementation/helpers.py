import csv


def readcsv(filepath = ''):
    file = open(filepath, "r")
    csvreader = csv.reader(file)
    input_data = []
    header = next(csvreader)
    for i in csvreader:
        input_data.append(i)
    file.close()
    return (header, input_data)

def writecsv(header, product_list, filepath = ''):
    file = open(filepath, "w")
    header.insert(2, 'Product-SalesTax')
    header.insert(3, 'Product-FinalPrice')
    
    for fields in header:
        file.write(str(fields) + ", ")
    file.write('\n')

    for data in product_list:
        file.write(data.name + ", ")
        file.write(data.cost_price + ", ")
        file.write(str(data.sales_tax) + ", ")
        file.write(str(data.final_price) + ", ")
        file.write(data.country + ",")
        file.write('\n')
    file.close()