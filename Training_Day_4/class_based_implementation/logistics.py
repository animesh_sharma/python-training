from helpers import readcsv, writecsv
from product import Product


class Logistics:
    '''This is the Logistics class

    It contains functions to get and generate logistics data.
    '''
    def __init__(self):
        self.headers = []
        self.product_list = []

    def get_all_products(self):
        self.headers, input_data = readcsv('../logistics_data/input_logistics_data.csv')
        for field in input_data:
            self.product_list.append(Product(field[0], field[1], field[2]))

    def generate_logistics(self):
        self.get_all_products()
        writecsv(self.headers, self.product_list, '../logistics_data/output_logistics_data.csv')        