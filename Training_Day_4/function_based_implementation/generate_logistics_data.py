import csv


def generate_logistics():
    header, input_data = read_from_csv()
    write_in_csv(header, input_data)

def read_from_csv():
    file = open('../logistics_data/input_logistics_data.csv', "r")
    csvreader = csv.reader(file)
    input_data = []
    header = next(csvreader)
    for i in csvreader:
        input_data.append(i)
    file.close()
    return (header, input_data)

def write_in_csv(header, input_data):
    file = open('../logistics_data/output_logistics_data.csv', "w")
    header.insert(2, 'Product-SalesTax')
    header.insert(3, 'Product-FinalPrice')
    sales_tax = 0.18
    
    for fields in header:
        file.write(str(fields) + ", ")
    file.write('\n')

    for data in input_data:
        for index in range(len(data)):
            file.write(data[index] + ", ")
            if index == 1:
                file.write(str(sales_tax)+ ", ")
                file.write(str(int(data[index]) + sales_tax*int(data[index]))+ ", ")
        file.write('\n')
    file.close()

generate_logistics()