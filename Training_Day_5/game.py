'''This is the game module

It contains classes and main() to start the quiz game.
'''
from helpers import generate_randint, enter_qna


class Player:
    '''Player class containing information of individual player'''

    def __init__(self):
        self.points = 0
        self.qna_count = 0

    def get_points(self):
        '''This method is to get the points of a player'''
        return self.points

    def get_qna(self):
        '''This method is to get the qna count of a player'''
        return self.qna_count

    def increment_points(self):
        '''This method is used to increment the points of a player'''
        self.points += 1

    def increment_qna(self):
        '''This method is used to increment the questions answered by a player'''
        self.qna_count += 1


class QuizGame:
    '''QuizGame class containing methods to start, continue and end the game'''

    def __init__(self):
        self.qna_list = enter_qna()

    def greeting(self, player):
        '''This function is used to start game with greeting and proceed
        according to user response'''
        response = input(
            "Welcome to AskPython Quiz\nAre you ready to play the Quiz ? (yes/no) ")
        if response.strip().lower() == 'yes':
            self.start_game(player)
        else:
            self.ending_msg(player)

    def start_game(self, player):
        '''This method starts the game for a received player'''
        self.show_questions(player)

    def show_questions(self, player):
        '''This method shows questions for a received player'''
        while self.qna_list:
            index = generate_randint(len(self.qna_list))
            player.increment_qna()
            response = input(
                f"Question {player.get_qna()} : {self.qna_list[index][0]} ")
            if response.strip().lower() in self.qna_list[index]:
                player.increment_points()
                print("Correct!")
            else:
                print("Wrong Answer!")
            self.qna_list.pop(index)
        self.ending_msg(player)

    def ending_msg(self, player):
        '''This method shows game ending message'''
        if player.get_qna():
            print(
                "Thankyou for Playing this small quiz game, "
                f"you attempted {player.get_points()} question(s) correctly!")
            print(f"Marks obtained: {int(player.get_points()*33.33)+1}")
        print("BYE!!")


def main():
    '''Main method used to initiate the game'''
    player = Player()
    quiz = QuizGame()
    quiz.greeting(player)


if __name__ == "__main__":
    main()
