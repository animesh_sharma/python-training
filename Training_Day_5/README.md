# A Quiz Game in Python

Foobar is a Python library for dealing with word pluralization.

## Installation
### Example Game Output:

Welcome to AskPython Quiz

Are you ready to play the Quiz ? (yes/no) :yes

Question 1: What is your Favourite programming language?python

correct

Question 2: Do you follow any author on AskPython? yes

correct

Question 3: What is the name of your favourite website for learning Python?askpython

correct

Thankyou for Playing this small quiz game, you attempted 3 questions correctly!

Marks obtained: 100.0

BYE!

## Should follow the following things:
1. Maintain a hard coded questions queue and give random questions one by one.

2. Correct Answers should be predefined for each question.

3. Follow all python python practice.

4. Write a class based game.