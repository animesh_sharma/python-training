'''This is the helpers module

This module contains general functions that can be used anywhere in the program.
'''


def generate_randint(max_int: int):
    '''This is the generate_randint function

    It is used to generate random integers within a range from 0 to max_int.
    '''
    dummy_object = {}
    helper_comp = 25214903917
    helper_prime = 11
    obj_address = str(id(dummy_object))
    previous = int(obj_address[len(obj_address)//2]
                   ) ^ int(obj_address[len(obj_address)//2 + 1])
    res = helper_comp * previous + helper_prime
    return res % max_int


def enter_qna():
    '''This is enter_qna function

    It is used to enter the hardcoded list of questions into a list.
    '''
    return [("What is your Favourite programming language?", "python"),
            ("Do you follow any author on AskPython?", "yes"),
            ("What is the name of your favourite website for learning Python?", "askpython")]
